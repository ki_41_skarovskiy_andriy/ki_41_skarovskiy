<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="mtr_a(7:0)" />
        <signal name="mtr_b(7:0)" />
        <signal name="mtr_c(7:0)" />
        <signal name="XLXN_4(31:0)" />
        <signal name="XLXN_5(31:0)" />
        <signal name="XLXN_6(31:0)" />
        <signal name="XLXN_7(31:0)" />
        <signal name="XLXN_8(31:0)" />
        <signal name="res1(31:0)" />
        <signal name="res3(31:0)">
        </signal>
        <signal name="XLXN_13(31:0)" />
        <signal name="XLXN_14(31:0)" />
        <signal name="XLXN_15(31:0)" />
        <signal name="XLXN_16(31:0)" />
        <signal name="XLXN_17(31:0)" />
        <signal name="res2(31:0)" />
        <signal name="counter(3:0)" />
        <signal name="Control(19:0)" />
        <signal name="Res(31:0)" />
        <signal name="CE" />
        <signal name="Zero_bit" />
        <signal name="Control(19:18)" />
        <signal name="Control(17)" />
        <signal name="Control(16:13)" />
        <signal name="Control(12:11)" />
        <signal name="Control(10)" />
        <signal name="Control(9:6)" />
        <signal name="Control(5)" />
        <signal name="Control(4)" />
        <signal name="Control(3)" />
        <signal name="Control(2)" />
        <signal name="Control(1)" />
        <signal name="Control(0)" />
        <signal name="CLK" />
        <signal name="XLXN_21" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35(31:0)" />
        <signal name="XLXN_36" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40" />
        <signal name="XLXN_41" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44(31:0)" />
        <signal name="XLXN_45(31:0)" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47(31:0)" />
        <signal name="XLXN_48(31:0)" />
        <signal name="XLXN_49(31:0)" />
        <signal name="XLXN_50(31:0)" />
        <signal name="XLXN_51" />
        <port polarity="Input" name="mtr_a(7:0)" />
        <port polarity="Input" name="mtr_b(7:0)" />
        <port polarity="Input" name="mtr_c(7:0)" />
        <port polarity="Output" name="res1(31:0)" />
        <port polarity="Output" name="res2(31:0)" />
        <port polarity="Output" name="counter(3:0)" />
        <port polarity="Output" name="Control(19:0)" />
        <port polarity="Output" name="Res(31:0)" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="Zero_bit" />
        <port polarity="Input" name="CLK" />
        <blockdef name="MTR_Mux_4_32">
            <timestamp>2016-12-2T21:12:56</timestamp>
            <rect width="288" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="352" y="-364" height="24" />
            <line x2="416" y1="-352" y2="-352" x1="352" />
        </blockdef>
        <blockdef name="MTR_RAM_32">
            <timestamp>2016-12-2T21:21:2</timestamp>
            <rect width="256" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-364" height="24" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
        </blockdef>
        <blockdef name="MTR_RAM1">
            <timestamp>2016-12-2T21:22:8</timestamp>
            <rect width="256" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-364" height="24" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
        </blockdef>
        <blockdef name="MTR_Mux_32">
            <timestamp>2016-12-2T21:0:54</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="MTR_Mul32">
            <timestamp>2016-12-2T21:29:35</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="ADDER_32">
            <timestamp>2016-12-2T21:34:41</timestamp>
            <rect width="224" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="112" y2="112" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="256" y1="112" y2="112" style="linewidth:W" x1="288" />
        </blockdef>
        <blockdef name="MTR_ROM">
            <timestamp>2016-12-3T12:35:15</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="MTR_counter">
            <timestamp>2016-12-2T21:41:59</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="208" y2="208" x1="0" />
            <line x2="544" y1="144" y2="144" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="MTR_register32">
            <timestamp>2016-12-2T21:18:50</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="mtr_con_8_to_32">
            <timestamp>2016-12-10T10:48:36</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="MTR_Mux_4_32" name="XLXI_1">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(19:18)" name="s0(1:0)" />
            <blockpin name="data1(31:0)" />
            <blockpin signalname="XLXN_4(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_17(31:0)" name="data3(31:0)" />
            <blockpin signalname="res2(31:0)" name="data4(31:0)" />
            <blockpin signalname="XLXN_7(31:0)" name="data_res(31:0)" />
        </block>
        <block symbolname="MTR_Mux_4_32" name="XLXI_2">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(12:11)" name="s0(1:0)" />
            <blockpin signalname="XLXN_5(31:0)" name="data1(31:0)" />
            <blockpin signalname="XLXN_6(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_17(31:0)" name="data3(31:0)" />
            <blockpin signalname="res2(31:0)" name="data4(31:0)" />
            <blockpin signalname="XLXN_8(31:0)" name="data_res(31:0)" />
        </block>
        <block symbolname="MTR_RAM_32" name="XLXI_3">
            <blockpin signalname="Zero_bit" name="WE" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="Control(17)" name="OE" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(16:13)" name="A(3:0)" />
            <blockpin signalname="XLXN_7(31:0)" name="DI(31:0)" />
            <blockpin signalname="res1(31:0)" name="DQ(31:0)" />
        </block>
        <block symbolname="MTR_RAM1" name="XLXI_4">
            <blockpin signalname="Zero_bit" name="WE" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="Control(10)" name="OE" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(9:6)" name="A(3:0)" />
            <blockpin signalname="XLXN_8(31:0)" name="DI(31:0)" />
            <blockpin signalname="res3(31:0)" name="DQ(31:0)" />
        </block>
        <block symbolname="MTR_Mux_32" name="XLXI_5">
            <blockpin signalname="Control(0)" name="so" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="res1(31:0)" name="data1(31:0)" />
            <blockpin signalname="res3(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_13(31:0)" name="data_o(31:0)" />
        </block>
        <block symbolname="MTR_Mux_32" name="XLXI_6">
            <blockpin signalname="Control(1)" name="so" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="res1(31:0)" name="data1(31:0)" />
            <blockpin signalname="res3(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_14(31:0)" name="data_o(31:0)" />
        </block>
        <block symbolname="MTR_Mux_32" name="XLXI_7">
            <blockpin signalname="Control(2)" name="so" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="res1(31:0)" name="data1(31:0)" />
            <blockpin signalname="res3(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_15(31:0)" name="data_o(31:0)" />
        </block>
        <block symbolname="MTR_Mux_32" name="XLXI_8">
            <blockpin signalname="Control(3)" name="so" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="res1(31:0)" name="data1(31:0)" />
            <blockpin signalname="res3(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_16(31:0)" name="data_o(31:0)" />
        </block>
        <block symbolname="MTR_Mul32" name="XLXI_9">
            <blockpin signalname="XLXN_13(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_14(31:0)" name="b(31:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="XLXN_17(31:0)" name="p(31:0)" />
        </block>
        <block symbolname="ADDER_32" name="XLXI_10">
            <blockpin signalname="XLXN_15(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_16(31:0)" name="b(31:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="CE" name="ce" />
            <blockpin signalname="res2(31:0)" name="s(31:0)" />
        </block>
        <block symbolname="MTR_ROM" name="XLXI_18">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="counter(3:0)" name="A(3:0)" />
            <blockpin signalname="Control(19:0)" name="D(19:0)" />
        </block>
        <block symbolname="MTR_counter" name="XLXI_19">
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="counter(3:0)" name="q(3:0)" />
        </block>
        <block symbolname="MTR_register32" name="XLXI_20">
            <blockpin signalname="Control(5)" name="ld" />
            <blockpin signalname="Control(4)" name="clr" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="res3(31:0)" name="d(31:0)" />
            <blockpin signalname="Res(31:0)" name="q(31:0)" />
        </block>
        <block symbolname="mtr_con_8_to_32" name="XLXI_25">
            <blockpin signalname="mtr_a(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_6(31:0)" name="s32(31:0)" />
        </block>
        <block symbolname="mtr_con_8_to_32" name="XLXI_26">
            <blockpin signalname="mtr_b(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_4(31:0)" name="s32(31:0)" />
        </block>
        <block symbolname="mtr_con_8_to_32" name="XLXI_27">
            <blockpin signalname="mtr_c(7:0)" name="s8(7:0)" />
            <blockpin signalname="XLXN_5(31:0)" name="s32(31:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="1232" y="1136" name="XLXI_1" orien="R0">
        </instance>
        <instance x="3152" y="1120" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1968" y="1136" name="XLXI_3" orien="R0">
        </instance>
        <instance x="3888" y="1136" name="XLXI_4" orien="R0">
        </instance>
        <branch name="mtr_a(7:0)">
            <wire x2="496" y1="1280" y2="1280" x1="160" />
        </branch>
        <branch name="mtr_b(7:0)">
            <wire x2="496" y1="1424" y2="1424" x1="160" />
        </branch>
        <branch name="mtr_c(7:0)">
            <wire x2="480" y1="1568" y2="1568" x1="160" />
        </branch>
        <branch name="XLXN_4(31:0)">
            <wire x2="1072" y1="1424" y2="1424" x1="880" />
            <wire x2="1072" y1="976" y2="1424" x1="1072" />
            <wire x2="1232" y1="976" y2="976" x1="1072" />
        </branch>
        <branch name="XLXN_5(31:0)">
            <wire x2="2416" y1="1568" y2="1568" x1="864" />
            <wire x2="2416" y1="896" y2="1568" x1="2416" />
            <wire x2="3152" y1="896" y2="896" x1="2416" />
        </branch>
        <branch name="XLXN_6(31:0)">
            <wire x2="2400" y1="1280" y2="1280" x1="880" />
            <wire x2="2400" y1="960" y2="1280" x1="2400" />
            <wire x2="3152" y1="960" y2="960" x1="2400" />
        </branch>
        <branch name="XLXN_7(31:0)">
            <wire x2="1808" y1="784" y2="784" x1="1648" />
            <wire x2="1808" y1="784" y2="1104" x1="1808" />
            <wire x2="1968" y1="1104" y2="1104" x1="1808" />
        </branch>
        <branch name="XLXN_8(31:0)">
            <wire x2="3728" y1="768" y2="768" x1="3568" />
            <wire x2="3728" y1="768" y2="1104" x1="3728" />
            <wire x2="3888" y1="1104" y2="1104" x1="3728" />
        </branch>
        <branch name="res1(31:0)">
            <wire x2="2624" y1="1632" y2="1632" x1="768" />
            <wire x2="768" y1="1632" y2="1984" x1="768" />
            <wire x2="928" y1="1984" y2="1984" x1="768" />
            <wire x2="768" y1="1984" y2="2336" x1="768" />
            <wire x2="928" y1="2336" y2="2336" x1="768" />
            <wire x2="768" y1="2336" y2="2704" x1="768" />
            <wire x2="928" y1="2704" y2="2704" x1="768" />
            <wire x2="768" y1="2704" y2="3072" x1="768" />
            <wire x2="928" y1="3072" y2="3072" x1="768" />
            <wire x2="2624" y1="784" y2="784" x1="2352" />
            <wire x2="2624" y1="784" y2="1520" x1="2624" />
            <wire x2="2624" y1="1520" y2="1632" x1="2624" />
            <wire x2="2992" y1="1520" y2="1520" x1="2624" />
        </branch>
        <branch name="res3(31:0)">
            <wire x2="4416" y1="1680" y2="1680" x1="880" />
            <wire x2="880" y1="1680" y2="2048" x1="880" />
            <wire x2="928" y1="2048" y2="2048" x1="880" />
            <wire x2="880" y1="2048" y2="2400" x1="880" />
            <wire x2="928" y1="2400" y2="2400" x1="880" />
            <wire x2="880" y1="2400" y2="2768" x1="880" />
            <wire x2="928" y1="2768" y2="2768" x1="880" />
            <wire x2="880" y1="2768" y2="3136" x1="880" />
            <wire x2="928" y1="3136" y2="3136" x1="880" />
            <wire x2="4416" y1="784" y2="784" x1="4272" />
            <wire x2="5040" y1="784" y2="784" x1="4416" />
            <wire x2="4416" y1="784" y2="1680" x1="4416" />
        </branch>
        <branch name="XLXN_16(31:0)">
            <wire x2="1552" y1="2944" y2="2944" x1="1312" />
            <wire x2="1552" y1="2432" y2="2944" x1="1552" />
            <wire x2="2000" y1="2432" y2="2432" x1="1552" />
        </branch>
        <branch name="res2(31:0)">
            <wire x2="1024" y1="448" y2="1104" x1="1024" />
            <wire x2="1232" y1="1104" y2="1104" x1="1024" />
            <wire x2="2944" y1="448" y2="448" x1="1024" />
            <wire x2="4672" y1="448" y2="448" x1="2944" />
            <wire x2="4672" y1="448" y2="2432" x1="4672" />
            <wire x2="2944" y1="448" y2="1088" x1="2944" />
            <wire x2="3152" y1="1088" y2="1088" x1="2944" />
            <wire x2="3760" y1="2432" y2="2432" x1="2288" />
            <wire x2="4672" y1="2432" y2="2432" x1="3760" />
            <wire x2="3760" y1="2432" y2="2496" x1="3760" />
        </branch>
        <instance x="320" y="384" name="XLXI_18" orien="R0">
        </instance>
        <branch name="counter(3:0)">
            <wire x2="320" y1="352" y2="352" x1="304" />
            <wire x2="304" y1="352" y2="432" x1="304" />
            <wire x2="800" y1="432" y2="432" x1="304" />
            <wire x2="816" y1="432" y2="432" x1="800" />
            <wire x2="816" y1="432" y2="448" x1="816" />
            <wire x2="800" y1="352" y2="352" x1="736" />
            <wire x2="736" y1="352" y2="448" x1="736" />
            <wire x2="816" y1="448" y2="448" x1="736" />
            <wire x2="800" y1="416" y2="432" x1="800" />
            <wire x2="864" y1="416" y2="416" x1="800" />
            <wire x2="864" y1="416" y2="608" x1="864" />
            <wire x2="864" y1="608" y2="608" x1="848" />
        </branch>
        <branch name="Control(19:0)">
            <wire x2="656" y1="3392" y2="3392" x1="448" />
            <wire x2="704" y1="3392" y2="3392" x1="656" />
            <wire x2="800" y1="3392" y2="3392" x1="704" />
            <wire x2="848" y1="3392" y2="3392" x1="800" />
            <wire x2="4800" y1="3392" y2="3392" x1="848" />
            <wire x2="1104" y1="288" y2="288" x1="704" />
            <wire x2="1104" y1="288" y2="304" x1="1104" />
            <wire x2="1920" y1="304" y2="304" x1="1104" />
            <wire x2="1968" y1="304" y2="304" x1="1920" />
            <wire x2="3056" y1="304" y2="304" x1="1968" />
            <wire x2="3728" y1="304" y2="304" x1="3056" />
            <wire x2="3840" y1="304" y2="304" x1="3728" />
            <wire x2="4800" y1="304" y2="304" x1="3840" />
            <wire x2="4800" y1="304" y2="592" x1="4800" />
            <wire x2="4800" y1="592" y2="656" x1="4800" />
            <wire x2="4800" y1="656" y2="672" x1="4800" />
            <wire x2="4800" y1="672" y2="3392" x1="4800" />
        </branch>
        <instance x="272" y="464" name="XLXI_19" orien="R0">
        </instance>
        <instance x="5040" y="816" name="XLXI_20" orien="R0">
        </instance>
        <branch name="Res(31:0)">
            <wire x2="5696" y1="592" y2="592" x1="5424" />
        </branch>
        <iomarker fontsize="28" x="5696" y="592" name="Res(31:0)" orien="R0" />
        <branch name="CE">
            <wire x2="1776" y1="240" y2="240" x1="1376" />
            <wire x2="1776" y1="240" y2="848" x1="1776" />
            <wire x2="1968" y1="848" y2="848" x1="1776" />
            <wire x2="3648" y1="240" y2="240" x1="1776" />
            <wire x2="3648" y1="240" y2="848" x1="3648" />
            <wire x2="3888" y1="848" y2="848" x1="3648" />
            <wire x2="3696" y1="240" y2="240" x1="3648" />
            <wire x2="3696" y1="240" y2="2800" x1="3696" />
            <wire x2="2000" y1="2560" y2="2560" x1="1936" />
            <wire x2="1936" y1="2560" y2="2800" x1="1936" />
            <wire x2="3696" y1="2800" y2="2800" x1="1936" />
        </branch>
        <iomarker fontsize="28" x="1376" y="240" name="CE" orien="R180" />
        <branch name="Zero_bit">
            <wire x2="1808" y1="192" y2="192" x1="1360" />
            <wire x2="1808" y1="192" y2="208" x1="1808" />
            <wire x2="1872" y1="208" y2="208" x1="1808" />
            <wire x2="1872" y1="208" y2="784" x1="1872" />
            <wire x2="1968" y1="784" y2="784" x1="1872" />
            <wire x2="3920" y1="192" y2="192" x1="1808" />
            <wire x2="3920" y1="192" y2="688" x1="3920" />
            <wire x2="3824" y1="688" y2="784" x1="3824" />
            <wire x2="3888" y1="784" y2="784" x1="3824" />
            <wire x2="3920" y1="688" y2="688" x1="3824" />
        </branch>
        <iomarker fontsize="28" x="1360" y="192" name="Zero_bit" orien="R180" />
        <bustap x2="1104" y1="304" y2="400" x1="1104" />
        <branch name="Control(19:18)">
            <wire x2="1104" y1="400" y2="848" x1="1104" />
            <wire x2="1232" y1="848" y2="848" x1="1104" />
        </branch>
        <bustap x2="1920" y1="304" y2="400" x1="1920" />
        <branch name="Control(17)">
            <wire x2="1920" y1="400" y2="912" x1="1920" />
            <wire x2="1968" y1="912" y2="912" x1="1920" />
        </branch>
        <bustap x2="1968" y1="304" y2="400" x1="1968" />
        <branch name="Control(16:13)">
            <wire x2="1968" y1="464" y2="464" x1="1904" />
            <wire x2="1904" y1="464" y2="1040" x1="1904" />
            <wire x2="1968" y1="1040" y2="1040" x1="1904" />
            <wire x2="1968" y1="400" y2="464" x1="1968" />
        </branch>
        <bustap x2="3056" y1="304" y2="400" x1="3056" />
        <branch name="Control(12:11)">
            <wire x2="3056" y1="400" y2="832" x1="3056" />
            <wire x2="3152" y1="832" y2="832" x1="3056" />
        </branch>
        <bustap x2="3728" y1="304" y2="400" x1="3728" />
        <branch name="Control(10)">
            <wire x2="3728" y1="400" y2="464" x1="3728" />
            <wire x2="3792" y1="464" y2="464" x1="3728" />
            <wire x2="3792" y1="464" y2="912" x1="3792" />
            <wire x2="3888" y1="912" y2="912" x1="3792" />
        </branch>
        <bustap x2="3840" y1="304" y2="400" x1="3840" />
        <branch name="Control(9:6)">
            <wire x2="3840" y1="400" y2="1040" x1="3840" />
            <wire x2="3888" y1="1040" y2="1040" x1="3840" />
        </branch>
        <bustap x2="4896" y1="592" y2="592" x1="4800" />
        <bustap x2="4896" y1="656" y2="656" x1="4800" />
        <branch name="Control(5)">
            <wire x2="5040" y1="592" y2="592" x1="4896" />
        </branch>
        <branch name="Control(4)">
            <wire x2="5040" y1="656" y2="656" x1="4896" />
        </branch>
        <branch name="Control(3)">
            <wire x2="928" y1="2944" y2="2944" x1="848" />
            <wire x2="848" y1="2944" y2="3296" x1="848" />
        </branch>
        <branch name="Control(2)">
            <wire x2="928" y1="2576" y2="2576" x1="800" />
            <wire x2="800" y1="2576" y2="3296" x1="800" />
        </branch>
        <iomarker fontsize="28" x="160" y="672" name="CLK" orien="R180" />
        <instance x="496" y="1312" name="XLXI_25" orien="R0">
        </instance>
        <instance x="496" y="1456" name="XLXI_26" orien="R0">
        </instance>
        <instance x="480" y="1600" name="XLXI_27" orien="R0">
        </instance>
        <iomarker fontsize="28" x="160" y="1280" name="mtr_a(7:0)" orien="R180" />
        <iomarker fontsize="28" x="160" y="1424" name="mtr_b(7:0)" orien="R180" />
        <iomarker fontsize="28" x="160" y="1568" name="mtr_c(7:0)" orien="R180" />
        <iomarker fontsize="28" x="800" y="352" name="counter(3:0)" orien="R0" />
        <branch name="CLK">
            <wire x2="208" y1="672" y2="672" x1="160" />
            <wire x2="272" y1="672" y2="672" x1="208" />
            <wire x2="208" y1="672" y2="992" x1="208" />
            <wire x2="928" y1="992" y2="992" x1="208" />
            <wire x2="208" y1="992" y2="1920" x1="208" />
            <wire x2="928" y1="1920" y2="1920" x1="208" />
            <wire x2="208" y1="1920" y2="2272" x1="208" />
            <wire x2="928" y1="2272" y2="2272" x1="208" />
            <wire x2="208" y1="2272" y2="2640" x1="208" />
            <wire x2="928" y1="2640" y2="2640" x1="208" />
            <wire x2="208" y1="2640" y2="3008" x1="208" />
            <wire x2="928" y1="3008" y2="3008" x1="208" />
            <wire x2="208" y1="3008" y2="3248" x1="208" />
            <wire x2="1408" y1="3248" y2="3248" x1="208" />
            <wire x2="1696" y1="3248" y2="3248" x1="1408" />
            <wire x2="320" y1="288" y2="288" x1="208" />
            <wire x2="208" y1="288" y2="672" x1="208" />
            <wire x2="928" y1="624" y2="784" x1="928" />
            <wire x2="928" y1="784" y2="992" x1="928" />
            <wire x2="1232" y1="784" y2="784" x1="928" />
            <wire x2="1744" y1="624" y2="624" x1="928" />
            <wire x2="1744" y1="624" y2="976" x1="1744" />
            <wire x2="1968" y1="976" y2="976" x1="1744" />
            <wire x2="3104" y1="624" y2="624" x1="1744" />
            <wire x2="3104" y1="624" y2="768" x1="3104" />
            <wire x2="3152" y1="768" y2="768" x1="3104" />
            <wire x2="3744" y1="624" y2="624" x1="3104" />
            <wire x2="4064" y1="624" y2="624" x1="3744" />
            <wire x2="4064" y1="624" y2="720" x1="4064" />
            <wire x2="5040" y1="720" y2="720" x1="4064" />
            <wire x2="3744" y1="624" y2="976" x1="3744" />
            <wire x2="3888" y1="976" y2="976" x1="3744" />
            <wire x2="1872" y1="2016" y2="2016" x1="1408" />
            <wire x2="1408" y1="2016" y2="3248" x1="1408" />
            <wire x2="1696" y1="2464" y2="3248" x1="1696" />
            <wire x2="2000" y1="2464" y2="2464" x1="1696" />
        </branch>
        <branch name="XLXN_13(31:0)">
            <wire x2="1872" y1="1856" y2="1856" x1="1312" />
        </branch>
        <branch name="XLXN_15(31:0)">
            <wire x2="1328" y1="2576" y2="2576" x1="1312" />
            <wire x2="2000" y1="2400" y2="2400" x1="1328" />
            <wire x2="1328" y1="2400" y2="2576" x1="1328" />
        </branch>
        <branch name="XLXN_14(31:0)">
            <wire x2="1552" y1="2208" y2="2208" x1="1312" />
            <wire x2="1552" y1="1920" y2="2208" x1="1552" />
            <wire x2="1872" y1="1920" y2="1920" x1="1552" />
        </branch>
        <branch name="Control(0)">
            <wire x2="928" y1="1856" y2="1856" x1="656" />
            <wire x2="656" y1="1856" y2="3296" x1="656" />
        </branch>
        <instance x="928" y="2080" name="XLXI_5" orien="R0">
        </instance>
        <instance x="1872" y="1776" name="XLXI_9" orien="R0">
        </instance>
        <branch name="XLXN_17(31:0)">
            <wire x2="1136" y1="544" y2="1040" x1="1136" />
            <wire x2="1232" y1="1040" y2="1040" x1="1136" />
            <wire x2="2880" y1="544" y2="544" x1="1136" />
            <wire x2="3040" y1="544" y2="544" x1="2880" />
            <wire x2="3040" y1="544" y2="1024" x1="3040" />
            <wire x2="3152" y1="1024" y2="1024" x1="3040" />
            <wire x2="2880" y1="544" y2="1856" x1="2880" />
            <wire x2="2880" y1="1856" y2="1856" x1="2448" />
        </branch>
        <iomarker fontsize="28" x="2992" y="1520" name="res1(31:0)" orien="R0" />
        <branch name="Control(1)">
            <wire x2="928" y1="2208" y2="2208" x1="704" />
            <wire x2="704" y1="2208" y2="3296" x1="704" />
        </branch>
        <instance x="928" y="2432" name="XLXI_6" orien="R0">
        </instance>
        <instance x="928" y="2800" name="XLXI_7" orien="R0">
        </instance>
        <instance x="928" y="3168" name="XLXI_8" orien="R0">
        </instance>
        <instance x="2000" y="2320" name="XLXI_10" orien="R0">
        </instance>
        <iomarker fontsize="28" x="3760" y="2496" name="res2(31:0)" orien="R90" />
        <iomarker fontsize="28" x="448" y="3392" name="Control(19:0)" orien="R180" />
        <bustap x2="656" y1="3392" y2="3296" x1="656" />
        <bustap x2="704" y1="3392" y2="3296" x1="704" />
        <bustap x2="800" y1="3392" y2="3296" x1="800" />
        <bustap x2="848" y1="3392" y2="3296" x1="848" />
    </sheet>
</drawing>