////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: MTR_REG.v
// /___/   /\     Timestamp: Sun Nov 20 20:34:38 2016
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -w -sim -ofmt verilog C:/Users/Taras/Desktop/MTR_Lab4/MTR_Lab4/ipcore_dir/tmp/_cg/MTR_REG.ngc C:/Users/Taras/Desktop/MTR_Lab4/MTR_Lab4/ipcore_dir/tmp/_cg/MTR_REG.v 
// Device	: 4vlx15sf363-12
// Input file	: C:/Users/Taras/Desktop/MTR_Lab4/MTR_Lab4/ipcore_dir/tmp/_cg/MTR_REG.ngc
// Output file	: C:/Users/Taras/Desktop/MTR_Lab4/MTR_Lab4/ipcore_dir/tmp/_cg/MTR_REG.v
// # of Modules	: 1
// Design Name	: MTR_REG
// Xilinx        : D:\XILINX\14.7\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module MTR_REG (
  clk, ce, sclr, q, d
)/* synthesis syn_black_box syn_noprune=1 */;
  input clk;
  input ce;
  input sclr;
  output [31 : 0] q;
  input [31 : 0] d;
  
  // synthesis translate_off
  
  wire \blk00000001/blk00000002/sig000000e8 ;
  wire \blk00000001/blk00000002/sig000000e7 ;
  wire \blk00000001/blk00000002/sig000000e6 ;
  wire \blk00000001/blk00000002/sig000000e5 ;
  wire \blk00000001/blk00000002/sig000000e4 ;
  wire \blk00000001/blk00000002/sig000000e3 ;
  wire \blk00000001/blk00000002/sig000000e2 ;
  wire \blk00000001/blk00000002/sig000000e1 ;
  wire \blk00000001/blk00000002/sig000000e0 ;
  wire \blk00000001/blk00000002/sig000000df ;
  wire \blk00000001/blk00000002/sig000000de ;
  wire \blk00000001/blk00000002/sig000000dd ;
  wire \blk00000001/blk00000002/sig000000dc ;
  wire \blk00000001/blk00000002/sig000000db ;
  wire \blk00000001/blk00000002/sig000000da ;
  wire \blk00000001/blk00000002/sig000000d9 ;
  wire \blk00000001/blk00000002/sig000000d8 ;
  wire \blk00000001/blk00000002/sig000000d7 ;
  wire \blk00000001/blk00000002/sig000000d6 ;
  wire \blk00000001/blk00000002/sig000000d5 ;
  wire \blk00000001/blk00000002/sig000000d4 ;
  wire \blk00000001/blk00000002/sig000000d3 ;
  wire \blk00000001/blk00000002/sig000000d2 ;
  wire \blk00000001/blk00000002/sig000000d1 ;
  wire \blk00000001/blk00000002/sig000000d0 ;
  wire \blk00000001/blk00000002/sig000000cf ;
  wire \blk00000001/blk00000002/sig000000ce ;
  wire \blk00000001/blk00000002/sig000000cd ;
  wire \blk00000001/blk00000002/sig000000cc ;
  wire \blk00000001/blk00000002/sig000000cb ;
  wire \blk00000001/blk00000002/sig000000ca ;
  wire \blk00000001/blk00000002/sig000000c9 ;
  wire \blk00000001/blk00000002/sig00000085 ;
  wire \blk00000001/blk00000002/sig00000084 ;
  wire \blk00000001/blk00000002/sig00000083 ;
  wire \blk00000001/blk00000002/sig00000082 ;
  wire \blk00000001/blk00000002/sig00000081 ;
  wire \blk00000001/blk00000002/sig00000080 ;
  wire \blk00000001/blk00000002/sig0000007f ;
  wire \blk00000001/blk00000002/sig0000007e ;
  wire \blk00000001/blk00000002/sig0000007d ;
  wire \blk00000001/blk00000002/sig0000007c ;
  wire \blk00000001/blk00000002/sig0000007b ;
  wire \blk00000001/blk00000002/sig0000007a ;
  wire \blk00000001/blk00000002/sig00000079 ;
  wire \blk00000001/blk00000002/sig00000078 ;
  wire \blk00000001/blk00000002/sig00000077 ;
  wire \blk00000001/blk00000002/sig00000076 ;
  wire \blk00000001/blk00000002/sig00000075 ;
  wire \blk00000001/blk00000002/sig00000074 ;
  wire \blk00000001/blk00000002/sig00000073 ;
  wire \blk00000001/blk00000002/sig00000072 ;
  wire \blk00000001/blk00000002/sig00000071 ;
  wire \blk00000001/blk00000002/sig00000070 ;
  wire \blk00000001/blk00000002/sig0000006f ;
  wire \blk00000001/blk00000002/sig0000006e ;
  wire \blk00000001/blk00000002/sig0000006d ;
  wire \blk00000001/blk00000002/sig0000006c ;
  wire \blk00000001/blk00000002/sig0000006b ;
  wire \blk00000001/blk00000002/sig0000006a ;
  wire \blk00000001/blk00000002/sig00000069 ;
  wire \blk00000001/blk00000002/sig00000068 ;
  wire \blk00000001/blk00000002/sig00000067 ;
  wire \blk00000001/blk00000002/sig00000066 ;
  wire \blk00000001/blk00000002/sig00000065 ;
  wire \blk00000001/blk00000002/sig00000064 ;
  wire \blk00000001/blk00000002/sig00000063 ;
  wire \blk00000001/blk00000002/sig00000062 ;
  wire \blk00000001/blk00000002/sig00000061 ;
  wire \blk00000001/blk00000002/sig00000060 ;
  wire \blk00000001/blk00000002/sig0000005f ;
  wire \blk00000001/blk00000002/sig0000005e ;
  wire \blk00000001/blk00000002/sig0000005d ;
  wire \blk00000001/blk00000002/sig0000005c ;
  wire \blk00000001/blk00000002/sig0000005b ;
  wire \blk00000001/blk00000002/sig0000005a ;
  wire \blk00000001/blk00000002/sig00000059 ;
  wire \blk00000001/blk00000002/sig00000058 ;
  wire \blk00000001/blk00000002/sig00000057 ;
  wire \blk00000001/blk00000002/sig00000056 ;
  wire \blk00000001/blk00000002/sig00000055 ;
  wire \blk00000001/blk00000002/sig00000054 ;
  wire \blk00000001/blk00000002/sig00000053 ;
  wire \blk00000001/blk00000002/sig00000052 ;
  wire \blk00000001/blk00000002/sig00000051 ;
  wire \blk00000001/blk00000002/sig00000050 ;
  wire \blk00000001/blk00000002/sig0000004f ;
  wire \blk00000001/blk00000002/sig0000004e ;
  wire \blk00000001/blk00000002/sig0000004d ;
  wire \blk00000001/blk00000002/sig0000004c ;
  wire \blk00000001/blk00000002/sig0000004b ;
  wire \blk00000001/blk00000002/sig0000004a ;
  wire \blk00000001/blk00000002/sig00000049 ;
  wire \blk00000001/blk00000002/sig00000048 ;
  wire \blk00000001/blk00000002/sig00000047 ;
  wire \blk00000001/blk00000002/sig00000046 ;
  wire \blk00000001/blk00000002/sig00000045 ;
  wire \blk00000001/blk00000002/sig00000044 ;
  wire \NLW_blk00000001/blk00000002/blk00000082_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000007f_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000007c_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000079_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000076_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000073_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000070_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000006d_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000006a_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000067_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000064_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000061_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000005e_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000005b_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000058_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000055_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000052_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000004f_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000004c_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000049_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000046_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000043_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000040_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000003d_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000003a_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000037_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000034_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000031_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000002e_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk0000002b_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000028_Q_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000002/blk00000025_Q_UNCONNECTED ;
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000084  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000075 ),
    .Q(\blk00000001/blk00000002/sig000000e2 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000083  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000074 ),
    .Q(\blk00000001/blk00000002/sig00000075 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000082  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[31]),
    .Q(\NLW_blk00000001/blk00000002/blk00000082_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000074 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000081  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000073 ),
    .Q(\blk00000001/blk00000002/sig000000e1 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000080  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000072 ),
    .Q(\blk00000001/blk00000002/sig00000073 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000007f  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[30]),
    .Q(\NLW_blk00000001/blk00000002/blk0000007f_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000072 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000007e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000006f ),
    .Q(\blk00000001/blk00000002/sig000000df )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000007d  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000006e ),
    .Q(\blk00000001/blk00000002/sig0000006f )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000007c  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[29]),
    .Q(\NLW_blk00000001/blk00000002/blk0000007c_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000006e )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000007b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000006d ),
    .Q(\blk00000001/blk00000002/sig000000de )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000007a  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000006c ),
    .Q(\blk00000001/blk00000002/sig0000006d )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000079  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[28]),
    .Q(\NLW_blk00000001/blk00000002/blk00000079_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000006c )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000078  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000006b ),
    .Q(\blk00000001/blk00000002/sig000000dd )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000077  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000006a ),
    .Q(\blk00000001/blk00000002/sig0000006b )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000076  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[27]),
    .Q(\NLW_blk00000001/blk00000002/blk00000076_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000006a )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000075  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000069 ),
    .Q(\blk00000001/blk00000002/sig000000dc )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000074  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000068 ),
    .Q(\blk00000001/blk00000002/sig00000069 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000073  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[26]),
    .Q(\NLW_blk00000001/blk00000002/blk00000073_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000068 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000072  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000067 ),
    .Q(\blk00000001/blk00000002/sig000000db )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000071  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000066 ),
    .Q(\blk00000001/blk00000002/sig00000067 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000070  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[25]),
    .Q(\NLW_blk00000001/blk00000002/blk00000070_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000066 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000006f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000065 ),
    .Q(\blk00000001/blk00000002/sig000000da )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000006e  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000064 ),
    .Q(\blk00000001/blk00000002/sig00000065 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000006d  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[24]),
    .Q(\NLW_blk00000001/blk00000002/blk0000006d_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000064 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000006c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000063 ),
    .Q(\blk00000001/blk00000002/sig000000d9 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000006b  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000062 ),
    .Q(\blk00000001/blk00000002/sig00000063 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000006a  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[23]),
    .Q(\NLW_blk00000001/blk00000002/blk0000006a_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000062 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000069  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000061 ),
    .Q(\blk00000001/blk00000002/sig000000d8 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000068  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000060 ),
    .Q(\blk00000001/blk00000002/sig00000061 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000067  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[22]),
    .Q(\NLW_blk00000001/blk00000002/blk00000067_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000060 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000066  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000005f ),
    .Q(\blk00000001/blk00000002/sig000000d7 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000065  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000005e ),
    .Q(\blk00000001/blk00000002/sig0000005f )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000064  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[21]),
    .Q(\NLW_blk00000001/blk00000002/blk00000064_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000005e )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000063  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000005d ),
    .Q(\blk00000001/blk00000002/sig000000d6 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000062  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000005c ),
    .Q(\blk00000001/blk00000002/sig0000005d )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000061  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[20]),
    .Q(\NLW_blk00000001/blk00000002/blk00000061_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000005c )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000060  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000059 ),
    .Q(\blk00000001/blk00000002/sig000000d4 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000005f  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000058 ),
    .Q(\blk00000001/blk00000002/sig00000059 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000005e  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[19]),
    .Q(\NLW_blk00000001/blk00000002/blk0000005e_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000058 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000005d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000057 ),
    .Q(\blk00000001/blk00000002/sig000000d3 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000005c  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000056 ),
    .Q(\blk00000001/blk00000002/sig00000057 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000005b  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[18]),
    .Q(\NLW_blk00000001/blk00000002/blk0000005b_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000056 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000005a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000055 ),
    .Q(\blk00000001/blk00000002/sig000000d2 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000059  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000054 ),
    .Q(\blk00000001/blk00000002/sig00000055 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000058  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[17]),
    .Q(\NLW_blk00000001/blk00000002/blk00000058_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000054 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000057  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000053 ),
    .Q(\blk00000001/blk00000002/sig000000d1 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000056  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000052 ),
    .Q(\blk00000001/blk00000002/sig00000053 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000055  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[16]),
    .Q(\NLW_blk00000001/blk00000002/blk00000055_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000052 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000054  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000051 ),
    .Q(\blk00000001/blk00000002/sig000000d0 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000053  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000050 ),
    .Q(\blk00000001/blk00000002/sig00000051 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000052  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[15]),
    .Q(\NLW_blk00000001/blk00000002/blk00000052_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000050 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000051  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000004f ),
    .Q(\blk00000001/blk00000002/sig000000cf )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000050  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000004e ),
    .Q(\blk00000001/blk00000002/sig0000004f )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000004f  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[14]),
    .Q(\NLW_blk00000001/blk00000002/blk0000004f_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000004e )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000004e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000004d ),
    .Q(\blk00000001/blk00000002/sig000000ce )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000004d  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000004c ),
    .Q(\blk00000001/blk00000002/sig0000004d )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000004c  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[13]),
    .Q(\NLW_blk00000001/blk00000002/blk0000004c_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000004c )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000004b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000004b ),
    .Q(\blk00000001/blk00000002/sig000000cd )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000004a  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000004a ),
    .Q(\blk00000001/blk00000002/sig0000004b )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000049  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[12]),
    .Q(\NLW_blk00000001/blk00000002/blk00000049_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000004a )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000048  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000049 ),
    .Q(\blk00000001/blk00000002/sig000000cc )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000047  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000048 ),
    .Q(\blk00000001/blk00000002/sig00000049 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000046  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[11]),
    .Q(\NLW_blk00000001/blk00000002/blk00000046_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000048 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000045  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000047 ),
    .Q(\blk00000001/blk00000002/sig000000cb )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000044  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000046 ),
    .Q(\blk00000001/blk00000002/sig00000047 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000043  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[10]),
    .Q(\NLW_blk00000001/blk00000002/blk00000043_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000046 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000042  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000083 ),
    .Q(\blk00000001/blk00000002/sig000000e8 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000041  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000082 ),
    .Q(\blk00000001/blk00000002/sig00000083 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000040  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[9]),
    .Q(\NLW_blk00000001/blk00000002/blk00000040_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000082 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000003f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000081 ),
    .Q(\blk00000001/blk00000002/sig000000e7 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000003e  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000080 ),
    .Q(\blk00000001/blk00000002/sig00000081 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000003d  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[8]),
    .Q(\NLW_blk00000001/blk00000002/blk0000003d_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000080 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000003c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000007f ),
    .Q(\blk00000001/blk00000002/sig000000e6 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000003b  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000007e ),
    .Q(\blk00000001/blk00000002/sig0000007f )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000003a  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[7]),
    .Q(\NLW_blk00000001/blk00000002/blk0000003a_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000007e )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000039  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000007d ),
    .Q(\blk00000001/blk00000002/sig000000e5 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000038  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000007c ),
    .Q(\blk00000001/blk00000002/sig0000007d )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000037  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[6]),
    .Q(\NLW_blk00000001/blk00000002/blk00000037_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000007c )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000036  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000007b ),
    .Q(\blk00000001/blk00000002/sig000000e4 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000035  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000007a ),
    .Q(\blk00000001/blk00000002/sig0000007b )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000034  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[5]),
    .Q(\NLW_blk00000001/blk00000002/blk00000034_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000007a )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000033  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000079 ),
    .Q(\blk00000001/blk00000002/sig000000e3 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000032  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000078 ),
    .Q(\blk00000001/blk00000002/sig00000079 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000031  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[4]),
    .Q(\NLW_blk00000001/blk00000002/blk00000031_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000078 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000030  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000077 ),
    .Q(\blk00000001/blk00000002/sig000000e0 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000002f  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000076 ),
    .Q(\blk00000001/blk00000002/sig00000077 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000002e  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[3]),
    .Q(\NLW_blk00000001/blk00000002/blk0000002e_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000076 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000002d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000071 ),
    .Q(\blk00000001/blk00000002/sig000000d5 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000002c  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000070 ),
    .Q(\blk00000001/blk00000002/sig00000071 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk0000002b  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[2]),
    .Q(\NLW_blk00000001/blk00000002/blk0000002b_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000070 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000002a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig0000005b ),
    .Q(\blk00000001/blk00000002/sig000000ca )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000029  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig0000005a ),
    .Q(\blk00000001/blk00000002/sig0000005b )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000028  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[1]),
    .Q(\NLW_blk00000001/blk00000002/blk00000028_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig0000005a )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000027  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig00000045 ),
    .Q(\blk00000001/blk00000002/sig000000c9 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000026  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000084 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/blk00000002/sig00000044 ),
    .Q(\blk00000001/blk00000002/sig00000045 )
  );
  SRLC16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000002/blk00000025  (
    .A0(\blk00000001/blk00000002/sig00000085 ),
    .A1(\blk00000001/blk00000002/sig00000085 ),
    .A2(\blk00000001/blk00000002/sig00000085 ),
    .A3(\blk00000001/blk00000002/sig00000085 ),
    .CE(ce),
    .CLK(clk),
    .D(d[0]),
    .Q(\NLW_blk00000001/blk00000002/blk00000025_Q_UNCONNECTED ),
    .Q15(\blk00000001/blk00000002/sig00000044 )
  );
  VCC   \blk00000001/blk00000002/blk00000024  (
    .P(\blk00000001/blk00000002/sig00000085 )
  );
  GND   \blk00000001/blk00000002/blk00000023  (
    .G(\blk00000001/blk00000002/sig00000084 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000022  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000e2 ),
    .R(sclr),
    .Q(q[31])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000021  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000e1 ),
    .R(sclr),
    .Q(q[30])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000020  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000df ),
    .R(sclr),
    .Q(q[29])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000001f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000de ),
    .R(sclr),
    .Q(q[28])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000001e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000dd ),
    .R(sclr),
    .Q(q[27])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000001d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000dc ),
    .R(sclr),
    .Q(q[26])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000001c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000db ),
    .R(sclr),
    .Q(q[25])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000001b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000da ),
    .R(sclr),
    .Q(q[24])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000001a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d9 ),
    .R(sclr),
    .Q(q[23])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000019  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d8 ),
    .R(sclr),
    .Q(q[22])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000018  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d7 ),
    .R(sclr),
    .Q(q[21])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000017  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d6 ),
    .R(sclr),
    .Q(q[20])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000016  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d4 ),
    .R(sclr),
    .Q(q[19])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000015  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d3 ),
    .R(sclr),
    .Q(q[18])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000014  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d2 ),
    .R(sclr),
    .Q(q[17])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000013  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d1 ),
    .R(sclr),
    .Q(q[16])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000012  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d0 ),
    .R(sclr),
    .Q(q[15])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000011  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000cf ),
    .R(sclr),
    .Q(q[14])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000010  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000ce ),
    .R(sclr),
    .Q(q[13])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000000f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000cd ),
    .R(sclr),
    .Q(q[12])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000000e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000cc ),
    .R(sclr),
    .Q(q[11])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000000d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000cb ),
    .R(sclr),
    .Q(q[10])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000000c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000e8 ),
    .R(sclr),
    .Q(q[9])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000000b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000e7 ),
    .R(sclr),
    .Q(q[8])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk0000000a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000e6 ),
    .R(sclr),
    .Q(q[7])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000009  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000e5 ),
    .R(sclr),
    .Q(q[6])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000008  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000e4 ),
    .R(sclr),
    .Q(q[5])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000007  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000e3 ),
    .R(sclr),
    .Q(q[4])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000006  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000e0 ),
    .R(sclr),
    .Q(q[3])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000005  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000d5 ),
    .R(sclr),
    .Q(q[2])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000004  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000ca ),
    .R(sclr),
    .Q(q[1])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000002/blk00000003  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/blk00000002/sig000000c9 ),
    .R(sclr),
    .Q(q[0])
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
