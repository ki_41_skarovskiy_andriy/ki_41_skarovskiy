----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:06:53 12/03/2016 
-- Design Name: 
-- Module Name:    MTR_ROM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.std_logic_arith.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MTR_ROM is
    Port ( CLK : in  STD_LOGIC;
           A : in  STD_LOGIC_VECTOR (3 downto 0);
           D : out  STD_LOGIC_VECTOR (19 downto 0));
end MTR_ROM;

architecture Behavioral of MTR_ROM is

begin
process (A)
		variable A_temp:integer;
		begin
		A_temp:= conv_integer(A(3 downto 0));	
				case A_temp is
					when 0 => D <= x"42010";	   				   				
					when 1 => D <= x"62411";	   				   				
					when 2 => D <= x"84850";     
					when 3 => D <= x"A4C58";     
					when 4 => D <= x"018D0";
					when 5 => D <= x"21CD2";
					when 6 => D <= x"01090";  	  			
					when 7 => D <= x"014A0";  	  							
					--when others => D <= "ZZZZZZZZZZZZZZZZZZZZ";
					when others => D <= "00000000000000000000";
				end case;			
	end process;
end Behavioral;

