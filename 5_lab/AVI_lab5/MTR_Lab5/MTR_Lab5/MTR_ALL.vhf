--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : MTR_ALL.vhf
-- /___/   /\     Timestamp : 12/18/2017 01:47:52
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -sympath D:/FPGA/MTR_Lab5/MTR_Lab5/ipcore_dir -intstyle ise -family virtex4 -flat -suppress -vhdl D:/FPGA/MTR_Lab5/MTR_Lab5/MTR_ALL.vhf -w D:/FPGA/MTR_Lab5/MTR_Lab5/MTR_ALL.sch
--Design Name: MTR_ALL
--Device: virtex4
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FD8CE_MXILINX_MTR_ALL is
   port ( C   : in    std_logic; 
          CE  : in    std_logic; 
          CLR : in    std_logic; 
          D   : in    std_logic_vector (7 downto 0); 
          Q   : out   std_logic_vector (7 downto 0));
end FD8CE_MXILINX_MTR_ALL;

architecture BEHAVIORAL of FD8CE_MXILINX_MTR_ALL is
   attribute BOX_TYPE   : string ;
   component FDCE
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCE : component is "BLACK_BOX";
   
begin
   I_Q0 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(0),
                Q=>Q(0));
   
   I_Q1 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(1),
                Q=>Q(1));
   
   I_Q2 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(2),
                Q=>Q(2));
   
   I_Q3 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(3),
                Q=>Q(3));
   
   I_Q4 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(4),
                Q=>Q(4));
   
   I_Q5 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(5),
                Q=>Q(5));
   
   I_Q6 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(6),
                Q=>Q(6));
   
   I_Q7 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(7),
                Q=>Q(7));
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FD16CE_MXILINX_MTR_ALL is
   port ( C   : in    std_logic; 
          CE  : in    std_logic; 
          CLR : in    std_logic; 
          D   : in    std_logic_vector (15 downto 0); 
          Q   : out   std_logic_vector (15 downto 0));
end FD16CE_MXILINX_MTR_ALL;

architecture BEHAVIORAL of FD16CE_MXILINX_MTR_ALL is
   attribute BOX_TYPE   : string ;
   component FDCE
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCE : component is "BLACK_BOX";
   
begin
   I_Q0 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(0),
                Q=>Q(0));
   
   I_Q1 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(1),
                Q=>Q(1));
   
   I_Q2 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(2),
                Q=>Q(2));
   
   I_Q3 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(3),
                Q=>Q(3));
   
   I_Q4 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(4),
                Q=>Q(4));
   
   I_Q5 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(5),
                Q=>Q(5));
   
   I_Q6 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(6),
                Q=>Q(6));
   
   I_Q7 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(7),
                Q=>Q(7));
   
   I_Q8 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(8),
                Q=>Q(8));
   
   I_Q9 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(9),
                Q=>Q(9));
   
   I_Q10 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(10),
                Q=>Q(10));
   
   I_Q11 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(11),
                Q=>Q(11));
   
   I_Q12 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(12),
                Q=>Q(12));
   
   I_Q13 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(13),
                Q=>Q(13));
   
   I_Q14 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(14),
                Q=>Q(14));
   
   I_Q15 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D(15),
                Q=>Q(15));
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ADD16_MXILINX_MTR_ALL is
   port ( A   : in    std_logic_vector (15 downto 0); 
          B   : in    std_logic_vector (15 downto 0); 
          CI  : in    std_logic; 
          CO  : out   std_logic; 
          OFL : out   std_logic; 
          S   : out   std_logic_vector (15 downto 0));
end ADD16_MXILINX_MTR_ALL;

architecture BEHAVIORAL of ADD16_MXILINX_MTR_ALL is
   attribute BOX_TYPE   : string ;
   attribute RLOC       : string ;
   signal C0       : std_logic;
   signal C1       : std_logic;
   signal C2       : std_logic;
   signal C3       : std_logic;
   signal C4       : std_logic;
   signal C5       : std_logic;
   signal C6       : std_logic;
   signal C7       : std_logic;
   signal C8       : std_logic;
   signal C9       : std_logic;
   signal C10      : std_logic;
   signal C11      : std_logic;
   signal C12      : std_logic;
   signal C13      : std_logic;
   signal C14      : std_logic;
   signal C14O     : std_logic;
   signal dummy    : std_logic;
   signal I0       : std_logic;
   signal I1       : std_logic;
   signal I2       : std_logic;
   signal I3       : std_logic;
   signal I4       : std_logic;
   signal I5       : std_logic;
   signal I6       : std_logic;
   signal I7       : std_logic;
   signal I8       : std_logic;
   signal I9       : std_logic;
   signal I10      : std_logic;
   signal I11      : std_logic;
   signal I12      : std_logic;
   signal I13      : std_logic;
   signal I14      : std_logic;
   signal I15      : std_logic;
   signal CO_DUMMY : std_logic;
   component FMAP
      port ( I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : in    std_logic);
   end component;
   attribute BOX_TYPE of FMAP : component is "BLACK_BOX";
   
   component MUXCY_L
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             LO : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY_L : component is "BLACK_BOX";
   
   component MUXCY
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY : component is "BLACK_BOX";
   
   component XORCY
      port ( CI : in    std_logic; 
             LI : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XORCY : component is "BLACK_BOX";
   
   component MUXCY_D
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             LO : out   std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY_D : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   attribute RLOC of I_36_16 : label is "X0Y4";
   attribute RLOC of I_36_17 : label is "X0Y4";
   attribute RLOC of I_36_18 : label is "X0Y5";
   attribute RLOC of I_36_19 : label is "X0Y5";
   attribute RLOC of I_36_20 : label is "X0Y6";
   attribute RLOC of I_36_21 : label is "X0Y6";
   attribute RLOC of I_36_22 : label is "X0Y7";
   attribute RLOC of I_36_23 : label is "X0Y7";
   attribute RLOC of I_36_55 : label is "X0Y4";
   attribute RLOC of I_36_58 : label is "X0Y5";
   attribute RLOC of I_36_62 : label is "X0Y5";
   attribute RLOC of I_36_63 : label is "X0Y6";
   attribute RLOC of I_36_64 : label is "X0Y7";
   attribute RLOC of I_36_107 : label is "X0Y7";
   attribute RLOC of I_36_110 : label is "X0Y6";
   attribute RLOC of I_36_111 : label is "X0Y4";
   attribute RLOC of I_36_248 : label is "X0Y3";
   attribute RLOC of I_36_249 : label is "X0Y3";
   attribute RLOC of I_36_250 : label is "X0Y2";
   attribute RLOC of I_36_251 : label is "X0Y2";
   attribute RLOC of I_36_252 : label is "X0Y1";
   attribute RLOC of I_36_253 : label is "X0Y1";
   attribute RLOC of I_36_254 : label is "X0Y0";
   attribute RLOC of I_36_255 : label is "X0Y0";
   attribute RLOC of I_36_272 : label is "X0Y0";
   attribute RLOC of I_36_275 : label is "X0Y0";
   attribute RLOC of I_36_279 : label is "X0Y1";
   attribute RLOC of I_36_283 : label is "X0Y1";
   attribute RLOC of I_36_287 : label is "X0Y2";
   attribute RLOC of I_36_291 : label is "X0Y2";
   attribute RLOC of I_36_295 : label is "X0Y3";
   attribute RLOC of I_36_299 : label is "X0Y3";
begin
   CO <= CO_DUMMY;
   I_36_16 : FMAP
      port map (I1=>A(8),
                I2=>B(8),
                I3=>dummy,
                I4=>dummy,
                O=>I8);
   
   I_36_17 : FMAP
      port map (I1=>A(9),
                I2=>B(9),
                I3=>dummy,
                I4=>dummy,
                O=>I9);
   
   I_36_18 : FMAP
      port map (I1=>A(10),
                I2=>B(10),
                I3=>dummy,
                I4=>dummy,
                O=>I10);
   
   I_36_19 : FMAP
      port map (I1=>A(11),
                I2=>B(11),
                I3=>dummy,
                I4=>dummy,
                O=>I11);
   
   I_36_20 : FMAP
      port map (I1=>A(12),
                I2=>B(12),
                I3=>dummy,
                I4=>dummy,
                O=>I12);
   
   I_36_21 : FMAP
      port map (I1=>A(13),
                I2=>B(13),
                I3=>dummy,
                I4=>dummy,
                O=>I13);
   
   I_36_22 : FMAP
      port map (I1=>A(14),
                I2=>B(14),
                I3=>dummy,
                I4=>dummy,
                O=>I14);
   
   I_36_23 : FMAP
      port map (I1=>A(15),
                I2=>B(15),
                I3=>dummy,
                I4=>dummy,
                O=>I15);
   
   I_36_55 : MUXCY_L
      port map (CI=>C8,
                DI=>A(9),
                S=>I9,
                LO=>C9);
   
   I_36_58 : MUXCY_L
      port map (CI=>C10,
                DI=>A(11),
                S=>I11,
                LO=>C11);
   
   I_36_62 : MUXCY_L
      port map (CI=>C9,
                DI=>A(10),
                S=>I10,
                LO=>C10);
   
   I_36_63 : MUXCY_L
      port map (CI=>C11,
                DI=>A(12),
                S=>I12,
                LO=>C12);
   
   I_36_64 : MUXCY
      port map (CI=>C14,
                DI=>A(15),
                S=>I15,
                O=>CO_DUMMY);
   
   I_36_73 : XORCY
      port map (CI=>C7,
                LI=>I8,
                O=>S(8));
   
   I_36_74 : XORCY
      port map (CI=>C8,
                LI=>I9,
                O=>S(9));
   
   I_36_75 : XORCY
      port map (CI=>C10,
                LI=>I11,
                O=>S(11));
   
   I_36_76 : XORCY
      port map (CI=>C9,
                LI=>I10,
                O=>S(10));
   
   I_36_77 : XORCY
      port map (CI=>C12,
                LI=>I13,
                O=>S(13));
   
   I_36_78 : XORCY
      port map (CI=>C11,
                LI=>I12,
                O=>S(12));
   
   I_36_80 : XORCY
      port map (CI=>C14,
                LI=>I15,
                O=>S(15));
   
   I_36_81 : XORCY
      port map (CI=>C13,
                LI=>I14,
                O=>S(14));
   
   I_36_107 : MUXCY_D
      port map (CI=>C13,
                DI=>A(14),
                S=>I14,
                LO=>C14,
                O=>C14O);
   
   I_36_110 : MUXCY_L
      port map (CI=>C12,
                DI=>A(13),
                S=>I13,
                LO=>C13);
   
   I_36_111 : MUXCY_L
      port map (CI=>C7,
                DI=>A(8),
                S=>I8,
                LO=>C8);
   
   I_36_226 : XORCY
      port map (CI=>CI,
                LI=>I0,
                O=>S(0));
   
   I_36_227 : XORCY
      port map (CI=>C0,
                LI=>I1,
                O=>S(1));
   
   I_36_228 : XORCY
      port map (CI=>C2,
                LI=>I3,
                O=>S(3));
   
   I_36_229 : XORCY
      port map (CI=>C1,
                LI=>I2,
                O=>S(2));
   
   I_36_230 : XORCY
      port map (CI=>C4,
                LI=>I5,
                O=>S(5));
   
   I_36_231 : XORCY
      port map (CI=>C3,
                LI=>I4,
                O=>S(4));
   
   I_36_233 : XORCY
      port map (CI=>C6,
                LI=>I7,
                O=>S(7));
   
   I_36_234 : XORCY
      port map (CI=>C5,
                LI=>I6,
                O=>S(6));
   
   I_36_248 : MUXCY_L
      port map (CI=>C6,
                DI=>A(7),
                S=>I7,
                LO=>C7);
   
   I_36_249 : MUXCY_L
      port map (CI=>C5,
                DI=>A(6),
                S=>I6,
                LO=>C6);
   
   I_36_250 : MUXCY_L
      port map (CI=>C4,
                DI=>A(5),
                S=>I5,
                LO=>C5);
   
   I_36_251 : MUXCY_L
      port map (CI=>C3,
                DI=>A(4),
                S=>I4,
                LO=>C4);
   
   I_36_252 : MUXCY_L
      port map (CI=>C2,
                DI=>A(3),
                S=>I3,
                LO=>C3);
   
   I_36_253 : MUXCY_L
      port map (CI=>C1,
                DI=>A(2),
                S=>I2,
                LO=>C2);
   
   I_36_254 : MUXCY_L
      port map (CI=>C0,
                DI=>A(1),
                S=>I1,
                LO=>C1);
   
   I_36_255 : MUXCY_L
      port map (CI=>CI,
                DI=>A(0),
                S=>I0,
                LO=>C0);
   
   I_36_272 : FMAP
      port map (I1=>A(1),
                I2=>B(1),
                I3=>dummy,
                I4=>dummy,
                O=>I1);
   
   I_36_275 : FMAP
      port map (I1=>A(0),
                I2=>B(0),
                I3=>dummy,
                I4=>dummy,
                O=>I0);
   
   I_36_279 : FMAP
      port map (I1=>A(2),
                I2=>B(2),
                I3=>dummy,
                I4=>dummy,
                O=>I2);
   
   I_36_283 : FMAP
      port map (I1=>A(3),
                I2=>B(3),
                I3=>dummy,
                I4=>dummy,
                O=>I3);
   
   I_36_287 : FMAP
      port map (I1=>A(4),
                I2=>B(4),
                I3=>dummy,
                I4=>dummy,
                O=>I4);
   
   I_36_291 : FMAP
      port map (I1=>A(5),
                I2=>B(5),
                I3=>dummy,
                I4=>dummy,
                O=>I5);
   
   I_36_295 : FMAP
      port map (I1=>A(6),
                I2=>B(6),
                I3=>dummy,
                I4=>dummy,
                O=>I6);
   
   I_36_299 : FMAP
      port map (I1=>A(7),
                I2=>B(7),
                I3=>dummy,
                I4=>dummy,
                O=>I7);
   
   I_36_354 : XOR2
      port map (I0=>A(0),
                I1=>B(0),
                O=>I0);
   
   I_36_355 : XOR2
      port map (I0=>A(1),
                I1=>B(1),
                O=>I1);
   
   I_36_356 : XOR2
      port map (I0=>A(2),
                I1=>B(2),
                O=>I2);
   
   I_36_357 : XOR2
      port map (I0=>A(3),
                I1=>B(3),
                O=>I3);
   
   I_36_358 : XOR2
      port map (I0=>A(4),
                I1=>B(4),
                O=>I4);
   
   I_36_359 : XOR2
      port map (I0=>A(5),
                I1=>B(5),
                O=>I5);
   
   I_36_360 : XOR2
      port map (I0=>A(6),
                I1=>B(6),
                O=>I6);
   
   I_36_361 : XOR2
      port map (I0=>A(7),
                I1=>B(7),
                O=>I7);
   
   I_36_362 : XOR2
      port map (I0=>A(8),
                I1=>B(8),
                O=>I8);
   
   I_36_363 : XOR2
      port map (I0=>A(9),
                I1=>B(9),
                O=>I9);
   
   I_36_364 : XOR2
      port map (I0=>A(10),
                I1=>B(10),
                O=>I10);
   
   I_36_365 : XOR2
      port map (I0=>A(11),
                I1=>B(11),
                O=>I11);
   
   I_36_366 : XOR2
      port map (I0=>A(12),
                I1=>B(12),
                O=>I12);
   
   I_36_367 : XOR2
      port map (I0=>A(13),
                I1=>B(13),
                O=>I13);
   
   I_36_368 : XOR2
      port map (I0=>A(14),
                I1=>B(14),
                O=>I14);
   
   I_36_369 : XOR2
      port map (I0=>A(15),
                I1=>B(15),
                O=>I15);
   
   I_36_375 : XOR2
      port map (I0=>C14O,
                I1=>CO_DUMMY,
                O=>OFL);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity MTR_SCH_FILTER_MUSER_MTR_ALL is
   port ( CE    : in    std_logic; 
          CLK   : in    std_logic; 
          CLR   : in    std_logic; 
          MTR_A : in    std_logic_vector (7 downto 0); 
          MTR_F : out   std_logic_vector (15 downto 0));
end MTR_SCH_FILTER_MUSER_MTR_ALL;

architecture BEHAVIORAL of MTR_SCH_FILTER_MUSER_MTR_ALL is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_4  : std_logic_vector (7 downto 0);
   signal XLXN_5  : std_logic_vector (7 downto 0);
   signal XLXN_6  : std_logic_vector (7 downto 0);
   signal XLXN_7  : std_logic_vector (7 downto 0);
   signal XLXN_8  : std_logic_vector (7 downto 0);
   signal XLXN_9  : std_logic_vector (7 downto 0);
   signal XLXN_41 : std_logic;
   signal XLXN_48 : std_logic_vector (15 downto 0);
   signal XLXN_49 : std_logic_vector (15 downto 0);
   signal XLXN_50 : std_logic_vector (15 downto 0);
   signal XLXN_51 : std_logic_vector (15 downto 0);
   signal XLXN_56 : std_logic_vector (15 downto 0);
   signal XLXN_63 : std_logic_vector (15 downto 0);
   signal XLXN_64 : std_logic_vector (15 downto 0);
   signal XLXN_67 : std_logic_vector (15 downto 0);
   signal XLXN_68 : std_logic_vector (15 downto 0);
   signal XLXN_69 : std_logic_vector (15 downto 0);
   signal XLXN_70 : std_logic_vector (15 downto 0);
   signal XLXN_71 : std_logic_vector (15 downto 0);
   signal XLXN_72 : std_logic;
   signal XLXN_73 : std_logic;
   signal XLXN_74 : std_logic;
   signal XLXN_75 : std_logic;
   signal XLXN_76 : std_logic;
   signal XLXN_77 : std_logic_vector (15 downto 0);
   component MTR_MUL_111
      port ( MTR_A   : in    std_logic_vector (7 downto 0); 
             MTR_OPT : out   std_logic_vector (15 downto 0));
   end component;
   
   component MTR_MUL_92
      port ( MTR_A   : in    std_logic_vector (7 downto 0); 
             MTR_OPT : out   std_logic_vector (15 downto 0));
   end component;
   
   component FD8CE_MXILINX_MTR_ALL
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic_vector (7 downto 0); 
             Q   : out   std_logic_vector (7 downto 0));
   end component;
   
   component ADD16_MXILINX_MTR_ALL
      port ( A   : in    std_logic_vector (15 downto 0); 
             B   : in    std_logic_vector (15 downto 0); 
             CI  : in    std_logic; 
             CO  : out   std_logic; 
             OFL : out   std_logic; 
             S   : out   std_logic_vector (15 downto 0));
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FD16CE_MXILINX_MTR_ALL
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic_vector (15 downto 0); 
             Q   : out   std_logic_vector (15 downto 0));
   end component;
   
   attribute HU_SET of XLXI_8 : label is "XLXI_8_27";
   attribute HU_SET of XLXI_9 : label is "XLXI_9_26";
   attribute HU_SET of XLXI_10 : label is "XLXI_10_38";
   attribute HU_SET of XLXI_11 : label is "XLXI_11_28";
   attribute HU_SET of XLXI_12 : label is "XLXI_12_29";
   attribute HU_SET of XLXI_13 : label is "XLXI_13_30";
   attribute HU_SET of XLXI_111 : label is "XLXI_111_31";
   attribute HU_SET of XLXI_112 : label is "XLXI_112_32";
   attribute HU_SET of XLXI_113 : label is "XLXI_113_33";
   attribute HU_SET of XLXI_114 : label is "XLXI_114_34";
   attribute HU_SET of XLXI_115 : label is "XLXI_115_35";
   attribute HU_SET of XLXI_116 : label is "XLXI_116_36";
   attribute HU_SET of XLXI_125 : label is "XLXI_125_37";
begin
   XLXI_1 : MTR_MUL_111
      port map (MTR_A(7 downto 0)=>XLXN_4(7 downto 0),
                MTR_OPT(15 downto 0)=>XLXN_64(15 downto 0));
   
   XLXI_2 : MTR_MUL_111
      port map (MTR_A(7 downto 0)=>MTR_A(7 downto 0),
                MTR_OPT(15 downto 0)=>XLXN_63(15 downto 0));
   
   XLXI_3 : MTR_MUL_111
      port map (MTR_A(7 downto 0)=>XLXN_5(7 downto 0),
                MTR_OPT(15 downto 0)=>XLXN_67(15 downto 0));
   
   XLXI_4 : MTR_MUL_92
      port map (MTR_A(7 downto 0)=>XLXN_6(7 downto 0),
                MTR_OPT(15 downto 0)=>XLXN_68(15 downto 0));
   
   XLXI_5 : MTR_MUL_92
      port map (MTR_A(7 downto 0)=>XLXN_7(7 downto 0),
                MTR_OPT(15 downto 0)=>XLXN_69(15 downto 0));
   
   XLXI_6 : MTR_MUL_92
      port map (MTR_A(7 downto 0)=>XLXN_8(7 downto 0),
                MTR_OPT(15 downto 0)=>XLXN_70(15 downto 0));
   
   XLXI_7 : MTR_MUL_92
      port map (MTR_A(7 downto 0)=>XLXN_9(7 downto 0),
                MTR_OPT(15 downto 0)=>XLXN_71(15 downto 0));
   
   XLXI_8 : FD8CE_MXILINX_MTR_ALL
      port map (C=>CLK,
                CE=>CE,
                CLR=>CLR,
                D(7 downto 0)=>XLXN_4(7 downto 0),
                Q(7 downto 0)=>XLXN_5(7 downto 0));
   
   XLXI_9 : FD8CE_MXILINX_MTR_ALL
      port map (C=>CLK,
                CE=>CE,
                CLR=>CLR,
                D(7 downto 0)=>MTR_A(7 downto 0),
                Q(7 downto 0)=>XLXN_4(7 downto 0));
   
   XLXI_10 : FD8CE_MXILINX_MTR_ALL
      port map (C=>CLK,
                CE=>CE,
                CLR=>CLR,
                D(7 downto 0)=>XLXN_5(7 downto 0),
                Q(7 downto 0)=>XLXN_6(7 downto 0));
   
   XLXI_11 : FD8CE_MXILINX_MTR_ALL
      port map (C=>CLK,
                CE=>CE,
                CLR=>CLR,
                D(7 downto 0)=>XLXN_6(7 downto 0),
                Q(7 downto 0)=>XLXN_7(7 downto 0));
   
   XLXI_12 : FD8CE_MXILINX_MTR_ALL
      port map (C=>CLK,
                CE=>CE,
                CLR=>CLR,
                D(7 downto 0)=>XLXN_7(7 downto 0),
                Q(7 downto 0)=>XLXN_8(7 downto 0));
   
   XLXI_13 : FD8CE_MXILINX_MTR_ALL
      port map (C=>CLK,
                CE=>CE,
                CLR=>CLR,
                D(7 downto 0)=>XLXN_8(7 downto 0),
                Q(7 downto 0)=>XLXN_9(7 downto 0));
   
   XLXI_111 : ADD16_MXILINX_MTR_ALL
      port map (A(15 downto 0)=>XLXN_63(15 downto 0),
                B(15 downto 0)=>XLXN_64(15 downto 0),
                CI=>XLXN_41,
                CO=>XLXN_72,
                OFL=>open,
                S(15 downto 0)=>XLXN_48(15 downto 0));
   
   XLXI_112 : ADD16_MXILINX_MTR_ALL
      port map (A(15 downto 0)=>XLXN_67(15 downto 0),
                B(15 downto 0)=>XLXN_48(15 downto 0),
                CI=>XLXN_72,
                CO=>XLXN_73,
                OFL=>open,
                S(15 downto 0)=>XLXN_49(15 downto 0));
   
   XLXI_113 : ADD16_MXILINX_MTR_ALL
      port map (A(15 downto 0)=>XLXN_68(15 downto 0),
                B(15 downto 0)=>XLXN_49(15 downto 0),
                CI=>XLXN_73,
                CO=>XLXN_74,
                OFL=>open,
                S(15 downto 0)=>XLXN_50(15 downto 0));
   
   XLXI_114 : ADD16_MXILINX_MTR_ALL
      port map (A(15 downto 0)=>XLXN_69(15 downto 0),
                B(15 downto 0)=>XLXN_50(15 downto 0),
                CI=>XLXN_74,
                CO=>XLXN_75,
                OFL=>open,
                S(15 downto 0)=>XLXN_51(15 downto 0));
   
   XLXI_115 : ADD16_MXILINX_MTR_ALL
      port map (A(15 downto 0)=>XLXN_70(15 downto 0),
                B(15 downto 0)=>XLXN_51(15 downto 0),
                CI=>XLXN_75,
                CO=>XLXN_76,
                OFL=>open,
                S(15 downto 0)=>XLXN_56(15 downto 0));
   
   XLXI_116 : ADD16_MXILINX_MTR_ALL
      port map (A(15 downto 0)=>XLXN_71(15 downto 0),
                B(15 downto 0)=>XLXN_56(15 downto 0),
                CI=>XLXN_76,
                CO=>open,
                OFL=>open,
                S(15 downto 0)=>XLXN_77(15 downto 0));
   
   XLXI_117 : GND
      port map (G=>XLXN_41);
   
   XLXI_125 : FD16CE_MXILINX_MTR_ALL
      port map (C=>CLK,
                CE=>CE,
                CLR=>CLR,
                D(15 downto 0)=>XLXN_77(15 downto 0),
                Q(15 downto 0)=>MTR_F(15 downto 0));
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity MTR_ALL is
   port ( CE         : in    std_logic; 
          CLK        : in    std_logic; 
          CLR        : in    std_logic; 
          MTR_A      : in    std_logic_vector (7 downto 0); 
          MTR_OUT_F  : out   std_logic_vector (15 downto 0); 
          MTR_OUT_IP : out   std_logic_vector (17 downto 0); 
          rdy        : out   std_logic; 
          rfd        : out   std_logic);
end MTR_ALL;

architecture BEHAVIORAL of MTR_ALL is
   component MTR_IP_FILTER
      port ( din  : in    std_logic_vector (7 downto 0); 
             dout : out   std_logic_vector (17 downto 0); 
             clk  : in    std_logic; 
             rfd  : out   std_logic; 
             rdy  : out   std_logic);
   end component;
   
   component MTR_SCH_FILTER_MUSER_MTR_ALL
      port ( MTR_A : in    std_logic_vector (7 downto 0); 
             CLK   : in    std_logic; 
             CE    : in    std_logic; 
             CLR   : in    std_logic; 
             MTR_F : out   std_logic_vector (15 downto 0));
   end component;
   
begin
   XLXI_10 : MTR_IP_FILTER
      port map (clk=>CLK,
                din(7 downto 0)=>MTR_A(7 downto 0),
                dout(17 downto 0)=>MTR_OUT_IP(17 downto 0),
                rdy=>rdy,
                rfd=>rfd);
   
   XLXI_11 : MTR_SCH_FILTER_MUSER_MTR_ALL
      port map (CE=>CE,
                CLK=>CLK,
                CLR=>CLR,
                MTR_A(7 downto 0)=>MTR_A(7 downto 0),
                MTR_F(15 downto 0)=>MTR_OUT_F(15 downto 0));
   
end BEHAVIORAL;


