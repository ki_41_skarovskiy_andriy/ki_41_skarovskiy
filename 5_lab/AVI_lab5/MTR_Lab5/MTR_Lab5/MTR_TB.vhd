-- Vhdl test bench created from schematic C:\Users\Taras\Desktop\MTR_Lab5\MTR_Lab5\MTR_ALL.sch - Sun Nov 27 19:35:34 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY MTR_ALL_MTR_ALL_sch_tb IS
END MTR_ALL_MTR_ALL_sch_tb;
ARCHITECTURE behavioral OF MTR_ALL_MTR_ALL_sch_tb IS 

   COMPONENT MTR_ALL
   PORT( MTR_OUT_F	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          MTR_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          CLK	:	IN	STD_LOGIC; 
			 CE	:	IN	STD_LOGIC; 
			 CLR	:	IN	STD_LOGIC; 
          MTR_OUT_IP	:	OUT	STD_LOGIC_VECTOR (17 DOWNTO 0); 
          rfd	:	OUT	STD_LOGIC; 
          rdy	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL MTR_OUT_F	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL MTR_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"00";
   SIGNAL CLK	:	STD_LOGIC:='0';
	SIGNAL CE	:	STD_LOGIC:='1';
	SIGNAL CLR	:	STD_LOGIC:='0';
   SIGNAL MTR_OUT_IP	:	STD_LOGIC_VECTOR (17 DOWNTO 0);
   SIGNAL rfd	:	STD_LOGIC;
   SIGNAL rdy	:	STD_LOGIC;
	constant clk_c : time := 20 ns;
	--Signal buf : integer := 0;

BEGIN

   UUT: MTR_ALL PORT MAP(
		MTR_OUT_F => MTR_OUT_F, 
		MTR_A => MTR_A, 
		CLK => CLK,
		CE => CE, 
		CLR => CLR, 
		MTR_OUT_IP => MTR_OUT_IP, 
		rfd => rfd, 
		rdy => rdy
   );


 	clk_process : PROCESS
BEGIN
	CLK <='0';
	WAIT FOR 20 ns;
	CLK <= '1';
	WAIT FOR 20 ns;
	END PROCESS;
	
	data_change_process : PROCESS
	VARIABLE I : INTEGER;
	
	BEGIN
	for i in 0 to 4 loop
	WAIT FOR 0 ns;
	MTR_A <= conv_std_logic_vector(conv_integer(MTR_A)+ 1, 8);
	WAIT FOR 30 ns;
	end loop;
	
	
	for i in 0 to 10 loop
	MTR_A<="00000000";
	wait for 40 ns;
	end loop;
	
	
	for i in 0 to 4 loop
	WAIT FOR 0 ns;
	MTR_A <= conv_std_logic_vector(conv_integer(MTR_A) + 4, 8);
	WAIT FOR 25 ns;
	end loop;
	
	for i in 0 to 10 loop
	MTR_A<="00000000";
	wait for 40 ns;
	end loop;
	END PROCESS;
	tb : PROCESS
	BEGIN
	WAIT; -- will wait forever
	END PROCESS;
END;




