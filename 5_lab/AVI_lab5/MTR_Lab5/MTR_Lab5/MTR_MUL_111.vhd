library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity MTR_MUL_111 is
	Port ( MTR_A : in  STD_LOGIC_VECTOR (7 downto 0);
           MTR_OPT : out  STD_LOGIC_VECTOR (15 downto 0));
end MTR_MUL_111;

architecture Behavioral of MTR_MUL_111 is
signal mtr_prod, mtr_p0, mtr_p4, mtr_p7:unsigned(15 downto 0);
begin

mtr_p0  <= "00000000" & unsigned(MTR_A);
mtr_p4  <= "0000" & unsigned(MTR_A) & "0000";
mtr_p7  <= "0" & unsigned(MTR_A) & "0000000";

mtr_prod <= (mtr_p7)-mtr_p4-mtr_p0;
MTR_OPT <= std_logic_vector (mtr_prod);

end Behavioral;

