<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="MTR_OUT_F(15:0)" />
        <signal name="MTR_A(7:0)" />
        <signal name="MTR_OUT_IP(17:0)" />
        <signal name="CLK" />
        <signal name="CE" />
        <signal name="CLR" />
        <signal name="rfd" />
        <signal name="rdy" />
        <port polarity="Output" name="MTR_OUT_F(15:0)" />
        <port polarity="Input" name="MTR_A(7:0)" />
        <port polarity="Output" name="MTR_OUT_IP(17:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="CLR" />
        <port polarity="Output" name="rfd" />
        <port polarity="Output" name="rdy" />
        <blockdef name="MTR_IP_FILTER">
            <timestamp>2016-11-30T14:10:22</timestamp>
            <rect width="512" x="32" y="32" height="248" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
            <line x2="32" y1="192" y2="192" x1="0" />
            <line x2="544" y1="160" y2="160" x1="576" />
            <line x2="544" y1="192" y2="192" x1="576" />
        </blockdef>
        <blockdef name="MTR_SCH_FILTER">
            <timestamp>2016-11-29T17:2:49</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <block symbolname="MTR_IP_FILTER" name="XLXI_10">
            <blockpin signalname="MTR_A(7:0)" name="din(7:0)" />
            <blockpin signalname="MTR_OUT_IP(17:0)" name="dout(17:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="rfd" name="rfd" />
            <blockpin signalname="rdy" name="rdy" />
        </block>
        <block symbolname="MTR_SCH_FILTER" name="XLXI_11">
            <blockpin signalname="MTR_A(7:0)" name="MTR_A(7:0)" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="MTR_OUT_F(15:0)" name="MTR_F(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="MTR_OUT_F(15:0)">
            <wire x2="1728" y1="320" y2="320" x1="1344" />
        </branch>
        <branch name="MTR_A(7:0)">
            <wire x2="800" y1="320" y2="320" x1="640" />
            <wire x2="960" y1="320" y2="320" x1="800" />
            <wire x2="800" y1="320" y2="704" x1="800" />
            <wire x2="896" y1="704" y2="704" x1="800" />
        </branch>
        <iomarker fontsize="28" x="640" y="320" name="MTR_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="640" y="384" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="1728" y="320" name="MTR_OUT_F(15:0)" orien="R0" />
        <branch name="MTR_OUT_IP(17:0)">
            <wire x2="1680" y1="704" y2="704" x1="1472" />
        </branch>
        <branch name="CLK">
            <wire x2="720" y1="384" y2="384" x1="640" />
            <wire x2="960" y1="384" y2="384" x1="720" />
            <wire x2="720" y1="384" y2="816" x1="720" />
            <wire x2="896" y1="816" y2="816" x1="720" />
        </branch>
        <instance x="896" y="624" name="XLXI_10" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1680" y="704" name="MTR_OUT_IP(17:0)" orien="R0" />
        <instance x="960" y="544" name="XLXI_11" orien="R0">
        </instance>
        <branch name="CE">
            <wire x2="960" y1="448" y2="448" x1="640" />
        </branch>
        <branch name="CLR">
            <wire x2="960" y1="512" y2="512" x1="640" />
        </branch>
        <iomarker fontsize="28" x="640" y="448" name="CE" orien="R180" />
        <iomarker fontsize="28" x="640" y="512" name="CLR" orien="R180" />
        <branch name="rfd">
            <wire x2="1488" y1="784" y2="784" x1="1472" />
            <wire x2="1728" y1="784" y2="784" x1="1488" />
        </branch>
        <branch name="rdy">
            <wire x2="1488" y1="816" y2="816" x1="1472" />
            <wire x2="1728" y1="816" y2="816" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="1728" y="784" name="rfd" orien="R0" />
        <iomarker fontsize="28" x="1728" y="816" name="rdy" orien="R0" />
    </sheet>
</drawing>