library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity MTR_MUL_92 is
	Port ( MTR_A : in  STD_LOGIC_VECTOR (7 downto 0);
           MTR_OPT : out  STD_LOGIC_VECTOR (15 downto 0));
end MTR_MUL_92;

architecture Behavioral of MTR_MUL_92 is
signal mtr_prod, mtr_p2, mtr_p5, mtr_p6:unsigned(15 downto 0);
begin

mtr_p2  <= "000000" & unsigned(MTR_A) & "00";
mtr_p5  <= "000" & unsigned(MTR_A) & "00000";
mtr_p6  <= "00" & unsigned(MTR_A) & "000000";

mtr_prod <= (mtr_p6 + mtr_p5)-mtr_p2;
MTR_OPT <= std_logic_vector (mtr_prod);

end Behavioral;

