----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:29:18 12/18/2017 
-- Design Name: 
-- Module Name:    AVI_mod1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AVI_mod1 is
    Port ( avi_a : in  STD_LOGIC;
           avi_b : in  STD_LOGIC;
           avi_c : in  STD_LOGIC;
           avi_d : in  STD_LOGIC;
           avi_e : in  STD_LOGIC;
           avi_res : out  STD_LOGIC);
end AVI_mod1;

architecture Behavioral of AVI_mod1 is

begin
avi_res<=((not(avi_a or avi_b) and ((not avi_b) xor avi_c)) or (avi_d and avi_e));

end Behavioral;

