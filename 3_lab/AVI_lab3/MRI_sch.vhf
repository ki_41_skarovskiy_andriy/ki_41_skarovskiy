--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : MRI_sch.vhf
-- /___/   /\     Timestamp : 11/14/2016 19:41:11
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -sympath E:/plis/MRI_3/ipcore_dir -intstyle ise -family spartan3a -flat -suppress -vhdl E:/plis/MRI_3/MRI_sch.vhf -w E:/plis/MRI_3/MRI_sch.sch
--Design Name: MRI_sch
--Device: spartan3a
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity MRI_sch is
   port ( MRI_A  : in    std_logic_vector (7 downto 0); 
          MRI_IP : out   std_logic_vector (39 downto 0); 
          MRI_NO : out   std_logic_vector (39 downto 0); 
          MRI_OP : out   std_logic_vector (39 downto 0));
end MRI_sch;

architecture BEHAVIORAL of MRI_sch is
   component MRI_1
      port ( MRI_A   : in    std_logic_vector (7 downto 0); 
             MRI_1_M : out   std_logic_vector (39 downto 0));
   end component;
   
   component MRI_2
      port ( MRI_A   : in    std_logic_vector (7 downto 0); 
             MRI_2_M : out   std_logic_vector (39 downto 0));
   end component;
   
   component MRI_P
      port ( a : in    std_logic_vector (7 downto 0); 
             p : out   std_logic_vector (39 downto 0));
   end component;
   
begin
   XLXI_1 : MRI_1
      port map (MRI_A(7 downto 0)=>MRI_A(7 downto 0),
                MRI_1_M(39 downto 0)=>MRI_NO(39 downto 0));
   
   XLXI_2 : MRI_2
      port map (MRI_A(7 downto 0)=>MRI_A(7 downto 0),
                MRI_2_M(39 downto 0)=>MRI_OP(39 downto 0));
   
   XLXI_3 : MRI_P
      port map (a(7 downto 0)=>MRI_A(7 downto 0),
                p(39 downto 0)=>MRI_IP(39 downto 0));
   
end BEHAVIORAL;


