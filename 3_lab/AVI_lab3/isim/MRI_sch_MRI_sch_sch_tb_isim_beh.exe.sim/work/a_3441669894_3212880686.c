/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "E:/plis/MRI_3/MRI_TB.vhd";
extern char *IEEE_P_1242562249;

char *ieee_p_1242562249_sub_1919365254_1035706684(char *, char *, char *, char *, int );


static void work_a_3441669894_3212880686_p_0(char *t0)
{
    char t12[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    int t9;
    int t10;
    int64 t11;
    char *t13;
    int t14;

LAB0:    t1 = (t0 + 2672U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(48, ng0);
    t2 = (t0 + 4884);
    t4 = (t0 + 3056);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 8U);
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(49, ng0);
    t2 = (t0 + 4892);
    *((int *)t2) = 1;
    t3 = (t0 + 4896);
    *((int *)t3) = 20;
    t9 = 1;
    t10 = 20;

LAB4:    if (t9 <= t10)
        goto LAB5;

LAB7:    xsi_set_current_line(55, ng0);

LAB19:    *((char **)t1) = &&LAB20;

LAB1:    return;
LAB5:    xsi_set_current_line(50, ng0);
    t11 = (5 * 1000LL);
    t4 = (t0 + 2480);
    xsi_process_wait(t4, t11);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB6:    t2 = (t0 + 4892);
    t9 = *((int *)t2);
    t3 = (t0 + 4896);
    t10 = *((int *)t3);
    if (t9 == t10)
        goto LAB7;

LAB16:    t14 = (t9 + 1);
    t9 = t14;
    t4 = (t0 + 4892);
    *((int *)t4) = t9;
    goto LAB4;

LAB8:    xsi_set_current_line(51, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 4684U);
    t4 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t12, t3, t2, 1);
    t5 = (t0 + 3056);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    memcpy(t13, t4, 8U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(52, ng0);
    t11 = (5 * 1000LL);
    t2 = (t0 + 2480);
    xsi_process_wait(t2, t11);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    goto LAB6;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB17:    goto LAB2;

LAB18:    goto LAB17;

LAB20:    goto LAB18;

}


extern void work_a_3441669894_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3441669894_3212880686_p_0};
	xsi_register_didat("work_a_3441669894_3212880686", "isim/MRI_sch_MRI_sch_sch_tb_isim_beh.exe.sim/work/a_3441669894_3212880686.didat");
	xsi_register_executes(pe);
}
