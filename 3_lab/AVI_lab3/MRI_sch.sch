<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3a" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(7:0)" />
        <signal name="XLXN_2(7:0)" />
        <signal name="MRI_A(7:0)" />
        <signal name="MRI_NO(39:0)" />
        <signal name="MRI_OP(39:0)" />
        <signal name="MRI_IP(39:0)" />
        <port polarity="Input" name="MRI_A(7:0)" />
        <port polarity="Output" name="MRI_NO(39:0)" />
        <port polarity="Output" name="MRI_OP(39:0)" />
        <port polarity="Output" name="MRI_IP(39:0)" />
        <blockdef name="MRI_1">
            <timestamp>2016-11-14T17:37:29</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="MRI_2">
            <timestamp>2016-11-14T17:37:38</timestamp>
            <rect width="304" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="MRI_P">
            <timestamp>2016-11-14T17:12:29</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="MRI_1" name="XLXI_1">
            <blockpin signalname="MRI_A(7:0)" name="MRI_A(7:0)" />
            <blockpin signalname="MRI_NO(39:0)" name="MRI_1_M(39:0)" />
        </block>
        <block symbolname="MRI_2" name="XLXI_2">
            <blockpin signalname="MRI_A(7:0)" name="MRI_A(7:0)" />
            <blockpin signalname="MRI_OP(39:0)" name="MRI_2_M(39:0)" />
        </block>
        <block symbolname="MRI_P" name="XLXI_3">
            <blockpin signalname="MRI_A(7:0)" name="a(7:0)" />
            <blockpin signalname="MRI_IP(39:0)" name="p(39:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1360" y="320" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1344" y="448" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1376" y="512" name="XLXI_3" orien="R0">
        </instance>
        <branch name="MRI_A(7:0)">
            <wire x2="1200" y1="288" y2="288" x1="1008" />
            <wire x2="1360" y1="288" y2="288" x1="1200" />
            <wire x2="1200" y1="288" y2="416" x1="1200" />
            <wire x2="1344" y1="416" y2="416" x1="1200" />
            <wire x2="1200" y1="416" y2="592" x1="1200" />
            <wire x2="1376" y1="592" y2="592" x1="1200" />
        </branch>
        <branch name="MRI_NO(39:0)">
            <wire x2="2352" y1="288" y2="288" x1="1792" />
        </branch>
        <branch name="MRI_OP(39:0)">
            <wire x2="2320" y1="416" y2="416" x1="1776" />
        </branch>
        <branch name="MRI_IP(39:0)">
            <wire x2="2336" y1="592" y2="592" x1="1952" />
        </branch>
        <iomarker fontsize="28" x="1008" y="288" name="MRI_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="2352" y="288" name="MRI_NO(39:0)" orien="R0" />
        <iomarker fontsize="28" x="2320" y="416" name="MRI_OP(39:0)" orien="R0" />
        <iomarker fontsize="28" x="2336" y="592" name="MRI_IP(39:0)" orien="R0" />
    </sheet>
</drawing>